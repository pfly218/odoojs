import api from '@/odoojs/index.js'

import { useWidget } from './userWidget'

import { computed, ref, toRaw } from 'vue'

export function useOne2many(props, ctx = {}) {
  const { emit } = ctx

  const useWidgetData = useWidget(props, { emit })
  const { readonly, onChange } = useWidgetData

  const RelationNode = computed(() => {
    if (!props.node) {
      return undefined
    }
    console.log(toRaw(props.node))
    return api.relation_node(props.node)
  })

  const Relation = computed(() => {
    if (!RelationNode.value) {
      return undefined
    }
    return RelationNode.value.relation
  })

  const datastore = computed(() => {
    const fname = props.node.name
    const { record_readonly = {}, values = {} } = props.formInfo || {}

    const records = record_readonly[fname] || []
    const values_me = values[fname] || []

    return {
      records,
      values: values_me
    }
  })

  function GetRelation() {
    const Rel = Relation.value
    if (!Rel) {
      return undefined
    }
    Rel.set_edit({
      ...datastore.value
    })

    return Rel
  }

  const value_display = computed(() => {
    // console.log(props.formInfo)
    const fname = props.node.name

    const { record } = props.formInfo || {}

    const val = record[fname]
    return val
  })

  const localState = {
    editmodel: undefined
  }

  const currentRow = ref({})
  const currentValues = ref({})

  async function rowNew() {
    currentRow.value = {}
    currentValues.value = {}

    const Rel = GetRelation()
    if (Rel) {
      const Editmodel = Rel.Editmodel
      Editmodel.set_edit({
        record: currentRow.value,
        values: currentValues.value,
        parent_info: props.formInfo || {}
      })

      const res2 = await Editmodel.onchange()
      console.log('rowNew', res2)

      const { values, domain, record_display } = res2
      currentRow.value = { ...record_display }
      currentValues.value = { ...values }

      localState.editmodel = Editmodel
    }
  }

  function rowPick(record) {
    currentRow.value = { ...record }
    currentValues.value = {}

    const Rel = GetRelation()

    if (Rel) {
      const Editmodel = Rel.Editmodel
      // console.log(props.formInfo)
      Editmodel.set_edit({
        record: currentRow.value,
        values: currentValues.value,
        parent_info: props.formInfo || {}
      })
      localState.editmodel = Editmodel
    }
  }

  async function rowChange(fname, value) {
    // console.log('onChange  ', fname, value)
    //
    const Editmodel = localState.editmodel
    if (!Editmodel) {
      return
    }

    const res3 = await Editmodel.onchange(fname, value)
    // console.log('rowChange', res3)
    const { values, domain, record_display } = res3
    currentRow.value = { ...record_display }
    currentValues.value = { ...values }
  }

  async function rowRemove() {
    const Rel = GetRelation()
    if (Rel) {
      const rid = currentRow.value.id

      const res = Rel.remove_one(rid)
      //   console.log('rowCommit', res)

      const records_edit = Rel.records_edit
      onChange(records_edit)

      currentRow.value = {}
      currentValues.value = {}
    }
  }

  async function rowCommit() {
    // todo 需要 进入 Editmodel 队列进行排队
    const Editmodel = localState.editmodel
    if (!Editmodel) {
      return
    }

    const Rel = GetRelation()
    if (Rel) {
      const res3 = await Editmodel.commit()
      // console.log('rowChange', res3)
      const { values, record_display } = res3
      currentRow.value = { ...record_display }
      currentValues.value = { ...values }

      const rid = currentRow.value.id
      const val = currentValues.value
      const res = Rel.upinsert_one(rid, val)
      //   console.log('rowCommit', res)

      const records_edit = Rel.records_edit
      //   console.log(records_edit)
      onChange(records_edit)

      currentRow.value = {}
      currentValues.value = {}
    }
  }

  return {
    readonly,
    value_display,

    currentRow,
    currentValues,
    rowNew,
    rowPick,
    rowChange,
    rowRemove,
    rowCommit
  }
}
