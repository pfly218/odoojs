export default {
  view_partner_tree: {
    arch: {
      sheet: {
        display_name: { string: '名称' },
        phone: { string: '电话' },
        email: { string: '邮箱' }
      }
    }
  },

  view_partner_form: {
    arch: {
      sheet: {
        _group: {
          _group_1: {
            name: {},
            vat: {}
          },

          _group_2: {
            phone: {},
            mobile: {},
            email: {}
          }
        }
      }
    }
  },

  view_partner_person_form: {
    arch: {
      sheet: {
        _group: {
          _group_1: {
            name: {},
            vat: {}
          },

          _group_2: {
            phone: {},
            mobile: {},
            email: {}
          }
        }
      }
    }
  }
}
