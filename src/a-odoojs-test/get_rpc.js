import { OdooJSRPC } from '@/odoojs-rpc/index.js'

const baseURL = process.env.VUE_APP_BASE_API
const timeout = 50000
const rpc = new OdooJSRPC({ baseURL, timeout })

export default rpc

const db = 'fp2'
const username = 'admin'
const password = '123456'

export async function login() {
  const payload = { db, login: username, password }
  const res = await rpc.login(payload)
  return res
}

export function sleep(millisecond) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}
