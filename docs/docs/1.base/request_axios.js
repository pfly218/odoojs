import axios from 'axios'

export class Request {
  constructor(payload) {
    const { baseURL, timeout = 500000, interceptors } = payload
    this._service = this._get_service({ baseURL, timeout, interceptors })
  }

  _get_service({ baseURL, timeout, interceptors }) {
    const service = axios.create({
      baseURL,
      // withCredentials: true,
      timeout
    })

    service.interceptors.request.use(
      config => {
        return interceptors.request.use(config)
      },
      error => {
        console.log('request error', error) // for debug
        return Promise.reject(error)
      }
    )

    service.interceptors.response.use(
      response => {
        return interceptors.response.use(response)
      },
      error => {
        console.log('request error', error) // for debug
        return Promise.reject(error)
      }
    )

    return service
  }

  async call_post(url, data) {
    const response = await this._service({ url, method: 'post', data })
    return response
  }
}
